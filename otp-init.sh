otp_dir=~/.local/share/otpsh/
acc_dir="$otp_dir/tokens/"

mkdir -v -- "$otp_dir"
mkdir -v -- "$acc_dir"
chmod -v 700 -- "$otp_dir"
