TOTP (time-based one-time password) authentication client for CLI.

To install the key from clipboard, run `otp-install $(wl-paste)`.
